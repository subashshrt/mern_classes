import React, { useState } from 'react';

const counter = ()=> {  
    const [counter, setCounter] = useState(0);
  const addCounter = () => setCounter(counter + 1);
  const subCounter = () => setCounter(counter - 1)
  return (
    <div>
      <button onClick={addCounter}> add</button>
      <div>{counter}</div>
      <button onClick={subCounter}> Subtract</button>
    </div>
  )
};
export default counter;
