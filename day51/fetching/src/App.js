import React, { Component } from 'react';
 
import './App.css';
import  {Switch, Route } from 'react-router-dom';
import HomeComponent from './components/HomeComponent';
import SearchComponent from './components/SearchComponent';
import AboutComponent from './components/AboutComponent';
import NavComponent from './components/NavComponent';
import ShowInfo from './components/ShowInfo';

class App extends Component {
  render() {
    return (
      <div >  
      <NavComponent/>
      <Switch>
        <Route exact path="/" component={HomeComponent}/> 
        <Route path="/Search" component={SearchComponent}/>
        <Route path="/About" component={AboutComponent}/>  
        <Route path="/Profile" component={ShowInfo}/>     

        </Switch> 
          
      </div>
    );
  }
}

export default App;
