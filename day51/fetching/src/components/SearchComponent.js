import React, { Component } from 'react'
import NavComponent from './NavComponent';
import ShowInfo from './ShowInfo';

export default class SearchComponent extends Component {
  constructor(){
    super();
    this.handleInputChange=this.handleInputChange.bind(this);
    this.getuser=this.getuser.bind(this);
  }

  state={
    res:[],
  }
 
 handleInputChange=(e)=>{
   e.preventDefault();
    fetch(`https://api.github.com/search/users?q=${e.target.search.value}`).then
   (response=>response.json()).then(data=>{
     console.log(data);
     this.setState({res:data.items})
   });
  // console.log(e.target.value);

 }
 getuser(username){
   console.log(username);
   this.props.history.push('/Profile',{"username":username})
 }

  
 
  render() {
    return (
      <div>
        <div className='row'>
            <div className="col-md-12">
                    <form onSubmit={this.handleInputChange} className="form-pos">
                      <input name="search" type="text" placeholder="Enter query for search" className="search"/> 
                  <button  type="submit"  className="search-button" >Search</button>  
                  </form>
              </div>
          </div>
        <div className="home-container">
              {this.state.res.map(user=>
              <div className="card" onClick={()=>this.getuser(user.login)}> 
                  <img src={user.avatar_url} className="card-img"/>
                  <p>{user.login}</p>
              </div>)}

          </div>
      </div>
    )
  }
}
