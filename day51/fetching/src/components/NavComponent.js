import React, { Component } from 'react';
import {NavLink} from 'react-router-dom';
import logo from '../1.png';
export default class NavComponent extends Component {
  render() {
    return (
      <div className="navbar navbar-black navbar-fix"> 
        <span className="navbar-items"> <img src={logo} className="icon"/> </span> 
         <b> <NavLink  to='/' style={{  textDecoration: 'none'}}><span className="navbar-items navbar-items-text" >Home</span></NavLink>
         <NavLink  to='/Search' style={{  textDecoration: 'none'}}><span className="navbar-items navbar-items-text" >Search</span></NavLink>
          <NavLink  to='/About' style={{  textDecoration: 'none'}}><span className="navbar-items navbar-items-text" >About</span></NavLink></b>
      </div>
    )
  }
}
