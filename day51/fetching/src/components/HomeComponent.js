import React, { Component } from 'react'
import NavComponent from './NavComponent';

export default class HomeComponent extends Component { 
    state={
        response:[],
    }
  
  componentDidMount(){
      fetch(`https://api.github.com/users`).then
      (response=>response.json()).then
      (data=> this.setState({response:data}));
  }
   render() {
    return (
      <div>
         {/* <NavComponent/> */}
         <div  className="home-container">
            { this.state.response.map(user=><Userlist user={user}/>)}
         </div>
      </div>
    )
  }
}
const Userlist =(props)=>{
    return(
        <div className="card" >
            <img src={props.user.avatar_url} className="card-img"/>
            <p>{props.user.login}</p>
        </div>
    )
}
