import React, { Component } from 'react'

export default class ShowInfo extends Component {

    state={
        info:'',
    }
    componentDidMount(){
       // console.log(this.props.location.state.username)
       fetch(`https://api.github.com/users/${this.props.location.state.username}`).then(res => res.json()).then
       (data=>this.setState( {info:data}));
    }
  render() {
    return (
     <div className="row intro-box">
     <div className="col-md-2"></div>
        <div className="col-md-4  ">
                <img src= {this.state.info.avatar_url } className="other-img"/>
                <p className="info-name"><b>{this.state.info.name}</b></p>
                <p className="info-login">{this.state.info.login}</p>
                <button type="button" className="follow-button" >Follow</button> 
         </div>
         <div className="col-md-4 description">
                <p className="info-desc">
                UserType:{this.state.info.type}<br/>
                Address:{this.state.info.location}<br/>
                </p>
         </div>
    </div>
          
    
    )
  }
}
