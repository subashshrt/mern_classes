import React, {Component, createContext} from 'react';
import items from './items.json';
import { element } from 'prop-types';
export const ProductContext = createContext();

export default class ProductContextProvider extends Component {
  state = {
    items:items.items,
    addedItems:[],
    totalAmount: 0
      
  }

  /*addItem = () => {
    this.setState(prevState => {
      return {
        count  : prevState.count + 1 
      
      }
   });*/
   addItem = (id) => {
    const Items = [...this.state.items];
    const foundItem=items.items.find(item=>item.id===id);
    const ind=this.state.items.indexOf(foundItem);
    console.log("index",ind);
    const newArr=[...this.state.items];
    // console.log(newArr[ind]['quantity']);
    newArr[ind]['quantity'] =newArr[ind]['quantity']+1;
    console.log(newArr[ind]['quantity']);
    this.setState({
      addedItems:newArr
    },()=>{
      console.log(this.state.addedItems)
    })
    this.setState(prevState => {
      return {
        totalAmount  : prevState.totalAmount + Items.find((item)=>item.id===id).price
      
      }
   });
    // this.setState({addedItems: [...this.state.addedItems, Items.find((item)=>item.id===id)]});
    // console.log(id);
  }

    

    

  removeItem = (id) => {
    const Items = [...this.state.items];
    const addedItm=[...this.state.addedItems]
    const index =  this.state.addedItems.indexOf(Items.find((item)=>item.id===id));
    console.log(index);
   this.setState({addedItems:  this.state.addedItm.splice(index,1,element)});
   this.setState(prevState => {
    return {
      totalAmount : prevState.totalAmount - Items.find((item)=>item.id===id).price
    
    }
 });

     
  }


  componentWillMount(){
    this.setState({items: items.items})
  }
  render() {
    return (
      <ProductContext.Provider value = {{state: this.state, addItem: this.addItem, count: this.state.count, removeItem: this.removeItem}}>
        {this.props.children}
      </ProductContext.Provider>
    )
  }
}