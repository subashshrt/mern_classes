import React, { Component } from 'react'
import '../items.json';
import { ProductContext } from '../ProductContextProvider.js';
export default class ItemsComponent extends Component {
  render() {
    return (
      
      <div>
         
         <h4><b>List of the items</b></h4>  <br/>
         <table><tr><th>Item name</th><th> Price</th><th>Actions</th></tr>
         <ProductContext.Consumer>
         {(value)=>(
               <div>{value.state.addedItems.map(item=> 
                  value.state.items.filter(allitem=> allitem.id===item.id).map(listitem=>
                    <div>
                        <tr>
                          <td>listitem.name</td>
                          <td>listitem.price</td>
                          <td><button className="btn btn-danger">Remove</button></td>
                        </tr>
                    </div>
                    )
                ) }</div>
           )}
         )}
          </ProductContext.Consumer>
          </table>
      </div>
    )
  }
}
