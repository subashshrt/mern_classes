import React from 'react';
import './App.css';
 
import ProductContextProvider from './ProductContextProvider';
import Checkout from './Components/Checkout';
import {Route, Switch} from 'react-router-dom';
import Section from './Components/Section';
export default () => 
    <ProductContextProvider>
        <Switch>
    <Route path = "/" component = {Section} exact={true}/>
    <Route path = "/checkout" component = {Checkout}  />
    </Switch>
    </ProductContextProvider>