import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import {ProductContext } from '../ProductContextProvider';
import {useContext} from 'react';
import Typography from '@material-ui/core/Typography'; 
import Button from '@material-ui/core/Button';
import { Link } from 'react-router-dom';
const styles = theme => ({
  root: {
    width: '60%',
    marginTop: theme.spacing.unit * 3,
    marginLeft: '20%',
    overflowX: 'auto',
  },
  table: {
    minWidth: 700,
  },
}); 
function SpanningTable(props) {
    const value = useContext(ProductContext)
  const { classes } = props;
  return (
    
    <Paper className={classes.root}>
         <Typography component="p">
        List of all the items
        </Typography>
      <Table className={classes.table}>
        <TableHead>
          <TableRow>
            <TableCell>Desc</TableCell>
            <TableCell align="right">Qty.</TableCell>
            <TableCell align="right">@</TableCell>
            <TableCell align="right">Price</TableCell>
            <TableCell align="right">Action</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
        {value.state.addedItems.map(item=>
            <TableRow key={item.id}>
              <TableCell>{item.name}</TableCell>
              <TableCell align="right">1</TableCell>
              <TableCell align="right">{item.price}</TableCell>
              <TableCell align="right">{item.price}</TableCell>
              <TableCell align="right"><Button variant="contained" color="secondary"     onClick={()=>value.removeItem(item.id)} >
          Remove
        </Button></TableCell>
            </TableRow>
          )}
          <TableRow>
            <TableCell rowSpan={3} />
            <TableCell colSpan={2}>Subtotal</TableCell>
            <TableCell align="right">{value.state.totalAmount}</TableCell>
          </TableRow>
           
          <TableRow>
            <TableCell colSpan={2}>Total</TableCell>
            <TableCell align="right">{value.state.totalAmount}</TableCell>
          </TableRow>
        </TableBody>
      
      
      </Table>
      <Button variant="contained" color="secondary" className="checkout">
        <Link to="/" onClick="window.location.reload()">Checkout</Link>
      </Button>
    </Paper>
  );
}

SpanningTable.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(SpanningTable);