import React, {useContext} from 'react';
import '../items.json';
import ItemCard from './ItemCard.js';
import {ProductContext} from '../ProductContextProvider';
import Navbar from './Navbar.js';

export default function Section() {
  const value = useContext(ProductContext);
  const {items} = value.state;
  return (
    <div>
       <div className="nav-pos"><Navbar/></div>
    <div className="section">
      {items.map((item)=><p><ItemCard item={item}/></p>)}
    </div>
    </div>
  )
}