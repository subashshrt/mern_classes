import React from 'react'
import Navbar from './Navbar';
import Section from './Section';
import  {BrowserRouter} from 'react-browser-router';
export default ()=> {
  return (
    <BrowserRouter>
    <div> 
      
      <Section/>
    </div>
    </BrowserRouter>
  )
}
