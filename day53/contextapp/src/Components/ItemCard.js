import React from 'react'; 
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import { ProductContext } from '../ProductContextProvider';
 
const styles = {
  card: {
    width: 400,
  },
  
};

function ItemCard(props) { 
  
  const { classes } = props;
  return (
     <ProductContext.Consumer>
       {(value)=>(  
    <Card className={classes.card}>
      <CardActionArea>
        <CardMedia
          component="img"
          alt="Vegetable"
          className={classes.media}
          height="200"
          image={`./images/${props.item.image}`}
          title={props.item.name}
        />
        <CardContent>
          <Typography gutterBottom variant="h5" component="h2">
            {props.item.name}
          </Typography>
          <Typography component="p">
            <b>Price:&nbsp;&nbsp;&nbsp;&nbsp;</b>Rs.{props.item.price}
          </Typography>
        </CardContent>
      </CardActionArea>
      <CardActions>
      <Button variant="contained" color="primary" id="add" className="addToCart" onClick={()=>value.addItem(props.item.id)} >
          Add to Cart
        </Button>
         
         
      </CardActions>
    </Card>
    )}
    </ProductContext.Consumer> 
  );
}



export default withStyles(styles)(ItemCard);