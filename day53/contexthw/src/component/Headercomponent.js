import React from 'react'
import { Themecontext } from '../Themeprovider';

export default ()=> {
  return (
      <Themecontext.Consumer>
          {(value)=>(
                <div className= {`header ${value.theme}`}>
                
              </div>
          )}
    
    </Themecontext.Consumer>
  )
}
