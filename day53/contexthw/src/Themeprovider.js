import React, { Component , createContext } from 'react'
export const Themecontext =  createContext();
export default class Themeprovider extends Component {
 
      state={
            theme: "light",
            checked: false
      }
      toggleTheme=()=>{
          this.state.theme ==="light"? this.setState({theme: "dark", checked: true}):this.setState({theme: "light", checked: false}) ;
      }
 
    render() {
    return (
       <Themecontext.Provider value={ {theme:this.state.theme , toggleTheme: this.toggleTheme, checked:this.state.checked}}>

           <this.state.children/>
       </Themecontext.Provider>

       
    )
  }
}
