import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Themeprovider from './Themeprovider';
import Headercomponent from './component/Headercomponent';

class App extends Component {
  render() {
    return (
      <Themeprovider>
      <div className="App">
         <Headercomponent/>
      </div>
      </Themeprovider>
    );
  }
}

export default App;
