//read the key that's pressed from the calculator
let value = '';
let bufferedValue = '';
let previousOperator = null;
let firstNumber = '';
let secondNumber = '';
let result = '';

let calculator = document.querySelector(".calculator");
let display = document.querySelector(".display");
calculator.addEventListener('click', function(e){
    value = e.target.innerText;
     
    processValue(value);
})

function processValue(value){
    if (isNaN(parseInt(value))){
        handleSymbol(value);
    } else {
        handleNumber(value);
    }
}

function handleSymbol(value){
    switch (value){
        case 'C':
            clearScreen();
        break;
        case '←':
            backSpace();
        break;
 
         
        case '=':
            if (previousOperator === null){
                return;
            } else {
                doMath(firstNumber);
            }
        break;
      default:
            storeValue(value);
    }
}

function backSpace(){
    if (display.innerText === '0'){
        clearScreen();
    }
    else if (Number.isInteger(bufferedValue) ){
        return;
    } else {
        bufferedValue = bufferedValue.substring(0, bufferedValue.length-1);
        console.log(bufferedValue);
        display.innerText = bufferedValue;
        if (bufferedValue === ''){
            clearScreen();
        }
    }
}

function clearScreen(){
    console.log('clearScreen');
    display.innerText = '0';
    bufferedValue = '';
}

 

function handleNumber(value){
    bufferedValue += value;
    console.log(bufferedValue);
    renderValue(bufferedValue);
}

function renderValue(bufferedValue){
    display.innerText = bufferedValue;
}

function storeValue(value){
    previousOperator = value;
    firstNumber = parseInt(bufferedValue);
    bufferedValue = '';
    console.log(firstNumber);
    console.log(previousOperator);
}

function doMath(firstNumber){
    console.log(firstNumber);
    secondNumber = parseInt(bufferedValue);
    console.log(secondNumber);
    if (previousOperator === '+'){
         
        bufferedValue = firstNumber + secondNumber;
        console.log(bufferedValue);
    }
     
    if (previousOperator === '-'){
        bufferedValue = firstNumber - secondNumber;
        console.log(bufferedValue);
    }
    if(previousOperator==='.')
    {
        bufferedValue=parse(firstNumber)+'.'+parse(secondNumber);
        bufferedValue= parseFloat(bufferedValue);

    }
    if (previousOperator === '×'){
        bufferedValue = firstNumber * secondNumber;
        console.log(bufferedValue);
    }
    if (previousOperator === '÷'){
        bufferedValue = firstNumber / secondNumber;
        console.log(bufferedValue);
    }
    if (previousOperator === '^'){
        bufferedValue = Math.pow(firstNumber,secondNumber);
        console.log(bufferedValue);
    }
    display.innerHTML = bufferedValue;
}

var d,h,m,s,animate;

function init(){
    d=new Date();
    h=d.getHours();
    m=d.getMinutes();
    s=d.getSeconds();
    clock();
};

function clock(){
    s++;
    if(s==60){
        s=0;
        m++;
        if(m==60){
            m=0;
            h++;
            if(h==24){
                h=0;
            }
        }
    }
    $('sec',s);
    $('min',m);
    $('hr',h);
    animate=setTimeout(clock,1000);
};

function $(id,val){
    if(val<10){
        val='0'+val;
    }
    document.getElementById(id).innerHTML=val;
};

window.onload=init;
