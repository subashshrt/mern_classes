import React, { Component } from 'react'; 
import './App.css';
import {Switch, Route} from 'react-router-dom';
import HomeComponent from './component/HomeComponent';
import SearchComponent from './component/SearchComponent';
import AboutComponent from './component/AboutComponent';
class App extends Component {
  render() {
    return (
      <div className="App">
          <Switch >
             <Route exact path="/" component={HomeComponent}  />
             <Route exact path="/Search" component={SearchComponent}/>
             <Route exact path="/About" component={AboutComponent} />

      </Switch>
      </div>
    );
  }
}

export default App;
      

