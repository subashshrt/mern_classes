import React, { Component } from 'react'
import NavComponent from './NavComponent';

export default class HomeComponent extends Component {
  state = {
    results : []
  }
  
  componentDidMount(){
    fetch('https://api.github.com/users?since=135').then(results=>{return results.json()}).then(data=>this.setState({results:data}))
    
  }  
  render() {
    return (
      <div>
           <NavComponent/>
           {this.state.results.map(user=>(
              <span className="panel panel-default">
                <span className="panel-heading" >  <img src={user.avatar_url} className="img-home panel-heading"/> </span> 
                 <span className="panel-body">  <span className="btn btn-primary  "> {user.login}</span></span>
            
           </span>
           ))}
      </div>
    )
  }
}
