import React, { Component } from 'react'
import SearchComponent from './SearchComponent';
import AboutComponent from './AboutComponent';
import {Link} from 'react-router-dom';
export default class NavComponent extends Component {
  render() {
    return (
      <div className="nav-container">
         
            
              <Link to="/"><button className="btn btn-primary nav-btn">Home</button></Link> 
              <Link to="/Search"><button className="btn btn-primary nav-btn">Search</button> </Link>
             <Link to="/About"> <button className="btn btn-primary nav-btn">About</button></Link>
              
           
      </div>
    )
  }
}
