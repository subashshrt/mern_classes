import React from 'react';

const Button =(props)=>{
    
    return(
        <div>
             
            <button className="btn btn-success btn-desc" onClick={ ()=>props.method(props.value) } >Add the items</button>
        </div>
    )
}
export default Button;