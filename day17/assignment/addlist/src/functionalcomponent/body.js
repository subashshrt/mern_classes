import React from "react"

const Body =(props) =>{
     
  
    return(
        <div >
          <button className="btn btn-primary btn-desc"> {props.objects.name}<span className="glyphicon glyphicon-remove glyph-position" onClick={()=>props.method(props.index)}></span></button>
        </div>
    )
}
export default Body;