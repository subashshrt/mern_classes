import React from 'react'
import Button from '../functionalcomponent/button';
import Body from '../functionalcomponent/body';
import Entry from '../functionalcomponent/entry';

export default class Controller extends React.Component{
  
constructor(){
    super();
    this.addname=this.addname.bind(this);
    this.deletename=this.deletename.bind(this);
}
state={
    subjects:[{
        name:"social",
        flag:true
    },
    {
        name:"science",
        flag:true

    }]
}

addname(value){
     
    this.setState(
        {
            subjects:[...this.state.subjects, {name:value,flag:true }]
        }
    )
}

deletename(index){
     
    let subjectsArr=[...this.state.subjects];
    //let newArr= subjectsArr.filter((subjects,i)=>i!==index);
    //subjectsArr.splice(index,1);


    this.setState({subjects:subjectsArr})
    
    }

     
 

    render(){
        return(
            <div>
                
               <span> <Entry method={this.addname}/>
                 <Button method={this.addname}  /></span>
               <span>  {this.state.subjects.map((subjects,i)=><Body objects={subjects} method={this.deletename} index={i}/>)}/></span>
            </div>
        )
    }
}