import React, { Component } from 'react';
 
import './App.css';
import Controller from './classcomponent/Controller';

class App extends Component {
   render() {
    return (
      <div className="App">
        <Controller/>
      </div>
    );
  }
}

export default App;
